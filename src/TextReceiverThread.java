
/*
 *
 * @author  abj
 */

import CMPC3M06.AudioPlayer;
import uk.ac.uea.cmp.voip.*;

import javax.sound.sampled.LineUnavailableException;
import java.io.*;
import java.net.*;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.CRC32;
import java.util.zip.Checksum;

public class TextReceiverThread implements Runnable {

    static DatagramSocket receiving_socket;
    AudioPlayer player = null;
    int socket, PORT;

    public void start(int newSocket, int newPORT) {
        this.socket = newSocket;
        this.PORT = newPORT;
        Thread thread = new Thread(this);
        thread.start();
    }

    public void newPlayer() {
        try {
            player = new AudioPlayer();
        } catch (LineUnavailableException ex) {
            ex.printStackTrace();
            System.exit(0);
        }
    }

    @Override
    public void run() {
        try {
            switch (socket) {
                case 1:
                    receiving_socket = new DatagramSocket(PORT);
                    newPlayer();
                    socket1();
                    break;
                case 2:
                    receiving_socket = new DatagramSocket2(PORT);
                    newPlayer();
                    socket2();
                    break;
                case 3:
                    receiving_socket = new DatagramSocket3(PORT);
                    newPlayer();
                    socket3();
                    break;
                case 4:
                    receiving_socket = new DatagramSocket4(PORT);
                    newPlayer();
                    socket4();
                    break;
            }

        } catch (SocketException e) {
            System.out.println("ERROR: TextReceiver: Could not open UDP socket to receive from.");
            e.printStackTrace();
            System.exit(0);
        } catch (IOException ex) {
            Logger.getLogger(TextReceiverThread.class.getName()).log(Level.SEVERE, null, ex);
        }

        //Close audio output
        player.close();

        //Close the socket
        receiving_socket.close();
    }

    public void socket1() throws IOException {
        boolean running = true;

        while (running) {

            try {
                // create a buffer which will store received packet
                final int SIZE = 516;
                byte[] buffer = new byte[SIZE];

                // create empty packet
                DatagramPacket packet = new DatagramPacket(buffer, SIZE);
                // receive packet
                receiving_socket.receive(packet);

                /**
                 * // get packet header and conver to int byte[] sequence = new
                 * byte[4]; System.arraycopy(buffer, 0, sequence, 0, 4); int
                 * sequenceNumber = ByteBuffer.wrap(sequence).getInt(); * //
                 * output sequence number System.out.println(sequenceNumber);
                 */
                //byte[] play = Arrays.copyOfRange(buffer, 4, SIZE);
                player.playBlock(buffer);
            } catch (IOException ex) {
                ex.printStackTrace();
                System.exit(0);
            }
        }

    }

    public void socket2() throws IOException {
        boolean running = true;
        final int SIZE = 516;
        final int WAIT = 4;
        byte[] lastPacket = new byte[SIZE];
        int lastSequence = 0;
        int repeats = 1;
        while (running) {

            try {
                ArrayList<byte[]> blockPackets = new ArrayList();

                for (int x = 0; x < WAIT; x++) {
                    // create a buffer which will store received packet
                    byte[] buffer = new byte[SIZE];
                    // create empty packet
                    DatagramPacket packet = new DatagramPacket(buffer, SIZE);
                    // receive packet
                    receiving_socket.receive(packet);
                    blockPackets.add(buffer);
                }

                Collections.sort(blockPackets, (byte[] o1, byte[] o2) -> {
                    int a = ByteBuffer.wrap(o1, 0, 4).getInt();
                    int b = ByteBuffer.wrap(o2, 0, 4).getInt();
                    return a - b;
                });

                for (byte[] b : blockPackets) {
                    // get packet header and convert to int
                    byte[] sequence = new byte[4];
                    System.arraycopy(b, 0, sequence, 0, 4);
                    int sequenceNumber = ByteBuffer.wrap(sequence).getInt();

                    byte[] sequence2 = new byte[4];
                    System.arraycopy(lastPacket, 0, sequence2, 0, 4);
                    lastSequence = ByteBuffer.wrap(sequence2).getInt();

                    if ((lastSequence + 1) != sequenceNumber && sequenceNumber != 0) {
                        repeats = sequenceNumber - lastSequence;
                    } else {
                        repeats = 1;
                    }
                    System.out.println("Repeats: " + repeats);
                    lastPacket = b;

                    /**
                     * 3 repeats occur when a packet of sequence number
                     * significantly greater than the last packet is played, we
                     * could play silence instead negative repeats occur when a
                     * packet of small sequence number is played after one of
                     * high sequence number, we could just play this one instead
                     * of repeats at the moment < 3 will get rid of the 3 repeats
                     * to keep 3 repeats do the following
                     * if(repeats > 0)
                     *
                     *
                     */
                    byte[] play = new byte[516];

                    if (repeats < 1) {
                        player.playBlock(play);
                        System.out.println("Silence: " + sequenceNumber);
                    } else {
                        play = Arrays.copyOfRange(b, 4, b.length);
                    }
                    for (int i = 0; i < repeats; i++) {
                        player.playBlock(play);
                        System.out.println(sequenceNumber);
                    }

                }

            } catch (IOException ex) {
                ex.printStackTrace();
                System.exit(0);
            }
        }

    }

    public void socket3() throws IOException {
        boolean running = true;
        final int SIZE = 516;
        final int INTERLEAVER = 2;
        final int ARRAY_SIZE = INTERLEAVER * INTERLEAVER;
        byte[] lastPacket = new byte[516];

        while (running) {

            try {
                ArrayList<byte[]> blockPackets = new ArrayList();

                for (int x = 0; x < ARRAY_SIZE + 1; x++) {
                    // create a buffer which will store received packet
                    byte[] buffer = new byte[SIZE];
                    // create empty packet
                    DatagramPacket packet = new DatagramPacket(buffer, SIZE);
                    // receive packet
                    receiving_socket.receive(packet);
                    blockPackets.add(buffer);
                }

                Collections.sort(blockPackets, (byte[] o1, byte[] o2) -> {
                    //gets header sequence number
                    int a = ByteBuffer.wrap(o1, 0, 4).getInt();
                    int b = ByteBuffer.wrap(o2, 0, 4).getInt();
                    return a - b;
                });

                for (byte[] b : blockPackets) {
                    // get packet header and conver to int
                    ByteBuffer buf = ByteBuffer.wrap(b);
                    int currentPacket = buf.getInt(0);

                    int lastPacketNum = ByteBuffer.wrap(lastPacket, 0, 4).getInt();
                    byte[] play = new byte[SIZE];
                    if (currentPacket - lastPacketNum == 1) {
                        // output sequence number
                        System.out.println(currentPacket);
                        play = Arrays.copyOfRange(b, 4, SIZE);
                        player.playBlock(play);
                    } else if (currentPacket - lastPacketNum < 0) {

                        // play current current packet
                        play = Arrays.copyOfRange(b, 4, SIZE);
                        player.playBlock(play);
                        System.out.println("Current: " + currentPacket);
                        // play last packet
                        play = Arrays.copyOfRange(lastPacket, 4, SIZE);
                        player.playBlock(play);
                        System.out.println("Last: " + lastPacketNum);
                    }
                    lastPacket = b;
                }

            } catch (IOException ex) {
                ex.printStackTrace();
                System.exit(0);
            }
        }

    }

    public void socket4() throws IOException {
        boolean running = true;
        final int SIZE = 524;
        byte[] lastPacket = new byte[SIZE];
        boolean lastPacketCorrupt = false;

        while (running) {

            try {
                Checksum checksum = new CRC32();
                // create a buffer which will store received packet
                byte[] buffer = new byte[SIZE];
                ByteBuffer buf = ByteBuffer.wrap(buffer);
                // create empty packet
                DatagramPacket packet = new DatagramPacket(buffer, SIZE);
                // receive packet
                receiving_socket.receive(packet);

                long received = buf.getLong();

                checksum.update(buffer, 12, buffer.length - 12);
                long checkSumVal = checksum.getValue();

                int sequenceNumber = buf.getInt(8);
                byte[] play = new byte[SIZE];
                int last = ByteBuffer.wrap(lastPacket, 8, 12).getInt();

                if (received == checkSumVal) {
                    if (lastPacketCorrupt) {
                        play = Arrays.copyOfRange(lastPacket, 8, lastPacket.length);
                        player.playBlock(play);
                        System.out.println("Repeat: " + last);

                    }
                    play = Arrays.copyOfRange(buffer, 12, buffer.length);
                    player.playBlock(play);
                    System.out.println(sequenceNumber);
                    lastPacketCorrupt = false;
                    lastPacket = buffer;
                } else {
                    lastPacketCorrupt = true;
                    System.out.println("Corrupt: " + sequenceNumber);
                }


            } catch (IOException ex) {
                ex.printStackTrace();
                System.exit(0);
            }
        }
    }
}