
/*
 * TextDuplex.java
 */

/**
 * @author abj
 */
public class TextDuplex {
    int socket;
    String IP;
    int PORT;
    TextReceiverThread receive;
    TextSenderThread send;

    public TextDuplex(int socket, String IP) {
        this.socket = socket;
        this.IP = IP;
        this.PORT = 55555;
        this.receive = new TextReceiverThread();
        this.send = new TextSenderThread();
    }

    public static void main(String[] args) {
        int socket = 3;
        TextDuplex voip = new TextDuplex(socket, "localhost");

        voip.send.start(voip.socket, voip.IP, voip.PORT);
        voip.receive.start(voip.socket, voip.PORT);

    }

}