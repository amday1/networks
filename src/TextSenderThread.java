import CMPC3M06.AudioRecorder;
import uk.ac.uea.cmp.voip.*;

import javax.sound.sampled.LineUnavailableException;
import java.io.*;
import java.net.*;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.zip.CRC32;
import java.util.zip.Checksum;

public class TextSenderThread implements Runnable {

    static DatagramSocket sending_socket;
    AudioRecorder recorder;
    InetAddress ipAddress;
    int PORT = 55555;
    int SOCKET;
    String IP;

    public void start(int SOCKET, String IP, int PORT) {
        this.PORT = PORT;
        this.SOCKET = SOCKET;
        this.IP = IP;
        Thread thread = new Thread(this);
        thread.start();
    }

    public void newRecorder() {
        try {
            recorder = new AudioRecorder();
        } catch (LineUnavailableException ex) {
            ex.printStackTrace();
            System.exit(0);
        }
    }

    public void run() {
        try {
            ipAddress = InetAddress.getByName(this.IP);
        } catch (UnknownHostException e) {
            System.out.println("ERROR: TextSender: Could not find client IP");
            e.printStackTrace();
            System.exit(0);
        }

        try {
            switch (SOCKET) {
                case 1:
                    sending_socket = new DatagramSocket();
                    newRecorder();
                    socket1();
                    break;
                case 2:
                    sending_socket = new DatagramSocket2();
                    newRecorder();
                    socket2();
                    break;
                case 3:
                    sending_socket = new DatagramSocket3();
                    newRecorder();
                    socket3();
                    break;
                case 4:
                    sending_socket = new DatagramSocket4();
                    newRecorder();
                    socket4();
                    break;
                default:
                    sending_socket = new DatagramSocket();
                    newRecorder();
                    socket1();
                    break;
            }

        } catch (SocketException e) {
            System.out
                    .println("ERROR: TextSender: Could not open UDP socket to send from.");
            e.printStackTrace();
            System.exit(0);
        }

        recorder.close();
        // Close the socket
        sending_socket.close();

    }

    public void socket1() {
        // int count = 1;
        boolean running = true;
        while (running) {
            try {
                byte[] data = recorder.getBlock();
                /**
                 * byte[] header = ByteBuffer.allocate(4).putInt(count).array();
                 *
                 * byte[] block = new byte[header.length + data.length];
                 * System.arraycopy(header, 0, block, 0, header.length);
                 * System.arraycopy(data, 0, block, header.length, data.length);
                 */
                DatagramPacket packet = new DatagramPacket(data, data.length,
                        ipAddress, PORT);
                sending_socket.send(packet);

            } catch (IOException e) {
                System.out
                        .println("ERROR: TextSender: Some random IO error occured!");
                e.printStackTrace();
                System.exit(0);
            }
            // count++;
        }
    }

    public void socket2() {
        int packetNum = 1;
        final int INTERLEAVER = 2;
        final int ARRAY_SIZE = INTERLEAVER * INTERLEAVER;
        final int SIZE = 516;
        boolean running = true;

        while (running) {
            // an array of arrays which will hold the interleaved block of
            // packets
            byte[][] interleavedPackets = new byte[ARRAY_SIZE][];

            try {
                ArrayList<byte[]> blockOfPackets = new ArrayList();
                for (int x = 0; x < ARRAY_SIZE; x++) {
                    byte[] header = ByteBuffer.allocate(4).putInt(packetNum)
                            .array();
                    byte[] data = recorder.getBlock();
                    byte[] block = new byte[header.length + data.length];
                    System.arraycopy(header, 0, block, 0, header.length);
                    System.arraycopy(data, 0, block, header.length, data.length);

                    blockOfPackets.add(block);
                    packetNum++;
                }

                // apply 90 degree interleave formula
                for (int i = 0; i < INTERLEAVER; i++) {
                    for (int j = 0; j < INTERLEAVER; j++) {
                        // i * d + j formula
                        int currentIndex = i * INTERLEAVER + j;
                        // j * d + (d - 1 - i)
                        int newIndex = j * INTERLEAVER + (INTERLEAVER - 1 - i);
                        interleavedPackets[newIndex] = blockOfPackets
                                .get(currentIndex);
                    }
                }

                for (int n = 0; n < ARRAY_SIZE; n++) {
                    byte[] blockSend = interleavedPackets[n];
                    DatagramPacket packet = new DatagramPacket(blockSend,
                            blockSend.length, ipAddress, PORT);
                    sending_socket.send(packet);
                }
            } catch (IOException e) {
                System.out
                        .println("ERROR: TextSender: Some random IO error occured!");
                e.printStackTrace();
                System.exit(0);
            }
        }
    }

    public void socket3() {
        int packetNum = 1;
        boolean running = true;
        while (running) {
            try {
                byte[] data = recorder.getBlock();

                byte[] header = ByteBuffer.allocate(4).putInt(packetNum)
                        .array();

                byte[] block = new byte[header.length + data.length];
                System.arraycopy(header, 0, block, 0, header.length);
                System.arraycopy(data, 0, block, header.length, data.length);

                DatagramPacket packet = new DatagramPacket(block, block.length,
                        ipAddress, PORT);
                sending_socket.send(packet);

            } catch (IOException e) {
                System.out
                        .println("ERROR: TextSender: Some random IO error occured!");
                e.printStackTrace();
                System.exit(0);
            }
            packetNum++;
        }
    }

    public void socket4() {
        int packetNum = 1;
        boolean running = true;
        while (running) {
            try {
                Checksum checksum = new CRC32();

                byte[] data = recorder.getBlock();
                byte[] block = new byte[4 + data.length + 8];

                ByteBuffer buf = ByteBuffer.wrap(block);

                checksum.update(data, 0, data.length);
                long checksumVal = checksum.getValue();

                // byte[] check =
                // ByteBuffer.allocate(8).putLong(checksumVal).array();
                // //puts checksum into block array
                // System.arraycopy(check, 0, block, 0, check.length);
                // //puts header into block array
                // System.arraycopy(packetNumber, 0, block, check.length,
                // packetNumber.length);
                // //puts data into block array
                // System.arraycopy(data, 0, block, packetNumber.length,
                // data.length);

                buf.putLong(checksumVal);
                buf.putInt(packetNum);
                buf.put(data);

                DatagramPacket packet = new DatagramPacket(block, block.length,
                        ipAddress, PORT);
                sending_socket.send(packet);

            } catch (IOException e) {
                System.out
                        .println("ERROR: TextSender: Some random IO error occured!");
                e.printStackTrace();
                System.exit(0);
            }
            packetNum++;
        }
    }
}