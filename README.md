# Networks

The aim of this assignment is to design, implement and evaluate a VoIP communication system to operate between two PCs in the Lewin Lab. 
The assignment is to be tackled in pairs – typically with one person responsible for the sender and the other for the receiver. 

This brings in issues such as the standardisation of VoIP packets. The design should follow a
layered approach with a suitable structure shown below.
The task of this coursework is to design and implement the VoIP layer. The audio layer is provided by the
AudioRecorder and AudioPlayer classes that are introduced in Lab 1. The transport layer should use the
DatagramSocket class in Java as the UDP access point which is covered in Lab 2.
The VoIP layer will need to be developed for the sending machine and the receiving machine. In its most basic operation
the VoIP layer on the sender will need to take audio blocks from the audio layer and pass them down into the transport
layer. On the receiving side the VoIP layer will receive packets from the transport layer and will need to pass them to the
audio layer ready for playback. This can be achieved by combining the work in Labs 1, 2, and 3.
Such a system should work well under ideal network conditions. However, in this coursework three other
DatagramSocket classes will be provided which simulate three non-ideal channel conditions:

• DatagramSocket2

• DatagramSocket3

• DatagramSocket4


In order for your VoIP system to operate effectively under these three unknown channels you must first analyse the
characteristics of the three channels and then design optimal systems for each channel. The analysis could be achieved by
sending packets across the network and monitoring what is received. Avoid using libraries for things such as CRC for
error detection – develop your own ideas to do this.
Once VoIP systems have been developed for the four network conditions (DatagramSocket, DatagramSocket2,
DatagramSocket3 and DatagramSocket4) an evaluation of the resulting Quality of Service should be made. This
should include parameters such as bit rate, delay, packet efficiency and a measurement of speech quality. You should
attempt to maximise QoS for each channel separately.